package pl.einstein.wdi.demorealtime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoRealTimeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoRealTimeApplication.class, args);
	}
}
