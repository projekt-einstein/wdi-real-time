package pl.einstein.wdi.demorealtime.model;

public class ChatMessage {

    private String author;
    private String content;

    public ChatMessage() {
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
