package pl.einstein.wdi.demorealtime.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import pl.einstein.wdi.demorealtime.model.ChatMessage;

@Controller
public class MessageCenterController {

    @MessageMapping(value = "/chat")
    @SendTo(value = "/queue")
    public ChatMessage chat(ChatMessage chatMessage) {
        return chatMessage;
    }

}
